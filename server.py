import asyncio
import os
from collections import defaultdict
from http import HTTPStatus
from pathlib import Path

import uvloop
from asyncpg import create_pool
from postgis.asyncpg import register
from query import OwlQuery
from roll import HttpError, Protocol, Roll
from roll.extensions import cors, simple_server, traceback

DB_CONFIG = {
    'database': os.environ.get('POSTGRES_DB', 'squirrel'),
    'host': os.environ.get('POSTGRES_HOST', None),
    'user': os.environ.get('POSTGRES_USER', None),
    'password': os.environ.get('POSTGRES_PASSWORD', None),
}


def load_sql_query(name):
    path = Path(__file__).parent / 'sql' / name
    with path.open() as f:
        return f.read()


POSITIONS_PER_REGISTRATION = load_sql_query('positions_per_registration.sql')
TRACKS_FOR = load_sql_query('tracks_for.sql')


class OwlProtocol(Protocol):
    Query = OwlQuery


class OwlRoll(Roll):
    Protocol = OwlProtocol


asyncio.set_event_loop(uvloop.new_event_loop())
app = OwlRoll()
cors(app)
traceback(app)


def jsonify(records):
    """
    Parse asyncpg record response into JSON format
    """
    yield from (dict(r.items()) for r in records)


@app.listen('startup')
async def register_db():
    app.pool = await create_pool(**DB_CONFIG, loop=app.loop, init=register)


@app.listen('shutdown')
async def close_connection():
    await app.pool.close()


@app.listen('error')
async def on_error(error, response):
    response.status = error.status
    response.json = {'error': error.message}


def filter_keys(payload, excludes):
    if not payload['allowed_at']:
        del payload['allowed']
    for key in excludes:
        del payload[key]
    return payload


@app.route('/positions/')
async def positions(request, response):
    async with app.pool.acquire() as connection:
        query = POSITIONS_PER_REGISTRATION.format(
            allowed=request.query.allowed)
        results = await connection.fetch(query, *request.query.bbox)
        results = [filter_keys(payload, excludes=['allowed_at'])
                   for payload in jsonify(results)]
        response.json = {'data': results}


@app.route('/tracks/')
async def tracks(request, response):
    async with app.pool.acquire() as connection:
        query = TRACKS_FOR.format(interval=request.query.interval,
                                  allowed=request.query.allowed,
                                  registration=request.query.registration)
        results = await connection.fetch(query, *request.query.bbox)
        data = defaultdict(list)
        for result in results:
            data[result['registration']].append(
                filter_keys(dict(result),
                            excludes=['allowed_at', 'registration']))
        response.json = {'data': data}


@app.route('/report/:error_hash')
async def report(request, response, error_hash):
    if len(error_hash) != 32:
        raise HttpError(HTTPStatus.BAD_REQUEST, 'Invalid report hash')
    async with app.pool.acquire() as connection:
        report = await connection.fetchrow(
            'SELECT errors, data FROM reports WHERE key=$1', error_hash)
        if not report:
            raise HttpError(HTTPStatus.NOT_FOUND, 'Unknown report hash')
        response.json = {
            'error': report['errors'],
            'data': bytes(report['data']).decode(errors='ignore')}


@app.route('/report')
async def reports(request, response):
    async with app.pool.acquire() as connection:
        reports = await connection.fetch(
            'SELECT errors, data FROM reports ORDER BY time DESC LIMIT 20')
        result = [{'error': str(report['errors']),
                   'data': str(report['data'])} for report in reports]
        response.json = {'data': result}


if __name__ == '__main__':
    simple_server(app, host='127.0.0.1', port=8080)
