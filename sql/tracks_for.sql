SELECT
    registration, coordinates, allowed, allowed_at,
    time AT TIME ZONE 'UTC' AS time
FROM
    data
WHERE
    coordinates && ST_MakeEnvelope($1, $2, $3, $4, 4326)
    AND {interval}
    AND {allowed}
    AND {registration}
ORDER BY
    registration, time ASC
LIMIT
    10000
