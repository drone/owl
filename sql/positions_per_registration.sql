SELECT
    time, registration, coordinates, allowed, allowed_at
FROM (
        SELECT
            registration, coordinates, allowed, allowed_at,
            time AT TIME ZONE 'UTC' AS time,
            row_number() OVER (
                PARTITION BY
                    registration
                ORDER BY
                    time DESC
            )
        FROM
            data
        WHERE
            coordinates && ST_MakeEnvelope($1, $2, $3, $4, 4326)
            AND time >= now() - '30 seconds'::interval
            AND {allowed}
    ) as tmp
WHERE
    row_number=1
ORDER BY
    time DESC
