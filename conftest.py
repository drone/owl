import asyncio
import json
from datetime import datetime, timezone

import asyncpg
import pytest
from postgis import Point
from postgis.asyncpg import register
from query import enforce_timezone
from server import app as myapp
from server import DB_CONFIG


def pytest_configure(config):

    async def main():
        conn = await asyncpg.connect(**DB_CONFIG)
        await register(conn)
        # TODO: find a way to factorize with squirrel?
        await conn.execute(
            'CREATE TABLE IF NOT EXISTS data ('
            '    id serial PRIMARY KEY,'
            '    coordinates geography(PointZ) NOT NULL,'
            '    time timestamp with time zone NOT NULL,'
            '    registration varchar(128) NOT NULL,'
            '    allowed boolean NULL DEFAULT true,'
            '    allowed_at timestamp with time zone NULL'
            ');')
        await conn.execute(
            'CREATE TABLE IF NOT EXISTS reports ('
            '    key varchar(32) PRIMARY KEY,'
            '    data bytea NOT NULL,'
            '    errors jsonb NOT NULL,'
            '    time timestamp with time zone NOT NULL'
            ');')
        await conn.close()

    asyncio.get_event_loop().run_until_complete(main())


def pytest_runtest_setup():

    async def main():
        conn = await asyncpg.connect(**DB_CONFIG)
        await conn.execute('TRUNCATE data;')
        await conn.execute('TRUNCATE reports;')
        await conn.close()

    asyncio.get_event_loop().run_until_complete(main())


@pytest.yield_fixture
async def connection():
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    yield conn
    await conn.close()


@pytest.yield_fixture
async def factory(connection):
    async def insert_payload(**payload):
        default = {
            'coordinates': [2.3497, 48.8042, 0],  # Paris
            'time': datetime.now(timezone.utc),
            'registration': 'test_foo',
            'allowed': True,
            'allowed_at': datetime.now(timezone.utc)
        }
        if 'time' in payload:
            payload['time'] = enforce_timezone(payload['time'])
        default.update(payload)
        point = Point(*default['coordinates'], srid=4326)
        time_ = default['time']
        registration = default['registration']
        allowed = default['allowed']
        allowed_at = default['allowed_at']
        await connection.execute(
            'INSERT INTO "data" '
            '    (coordinates, time, registration, allowed, allowed_at) '
            'VALUES ($1, $2, $3, $4, $5)',
            point, time_, registration, allowed, allowed_at)
        return default
    yield insert_payload


@pytest.yield_fixture
async def report(connection):
    async def insert_report(errors, key='*' * 32):
        errors = json.dumps(errors)
        await connection.execute(
            'INSERT INTO "reports" (key, data, errors, time) '
            'VALUES ($1, $2, $3, $4)',
            key, b'', errors, datetime.now(timezone.utc))
    yield insert_report


@pytest.fixture
def bbox():
    france_metro_bbox = -4.99, 41.34, 9.69, 51.04
    return 'west={}&south={}&east={}&north={}'.format(*france_metro_bbox)


@pytest.fixture
def app():
    return myapp
