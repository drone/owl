from datetime import timezone
from http import HTTPStatus

from dateutil import parser
from roll import HttpError, Query


def enforce_timezone(value):
    # Always compare dates in the same timezone,
    # either in the system timezone or in UTC.
    if not value.tzinfo:
        value = value.replace(tzinfo=timezone.utc)
    return value


class OwlQuery(Query):

    @property
    def bbox(self):
        if ('west' not in self or 'south' not in self or
                'east' not in self or 'north' not in self):
            message = (
                'A boundary box is required to filter returned results, '
                'you must provide both `west`, `south`, `east` and `north` '
                'parameters as floats within the [-180, -90, 180, 90] range '
                'respectively.'
            )
            raise HttpError(HTTPStatus.BAD_REQUEST, message)
        west = self.float('west')
        if not -180 <= west < 180:
            message = '`west` parameter must be within the [-180, 180] range.'
            raise HttpError(HTTPStatus.BAD_REQUEST, message)
        south = self.float('south')
        if not -90 <= south < 90:
            message = '`south` parameter must be within the [-90, 90] range.'
            raise HttpError(HTTPStatus.BAD_REQUEST, message)
        east = self.float('east')
        if not -180 < east <= 180:
            message = '`east` parameter must be within the [-180, 180] range.'
            raise HttpError(HTTPStatus.BAD_REQUEST, message)
        north = self.float('north')
        if not -90 < north <= 90:
            message = '`north` parameter must be within the [-90, 90] range.'
            raise HttpError(HTTPStatus.BAD_REQUEST, message)
        return west, south, east, north

    @property
    def allowed(self):
        if 'allowed' in self:
            allowed = self.bool('allowed')
            if allowed is None:
                return 'allowed IS NULL'
            elif allowed:
                return 'allowed'
            elif not allowed:
                return 'NOT allowed'
        return 'true'  # Inoffensive default.

    @property
    def interval(self):
        try:
            start, end = self.get('interval').split('/')
            start, end = parser.parse(start), parser.parse(end)
        except ValueError:
            message = (
                'The `interval` parameter must be a valid time interval: '
                'https://en.wikipedia.org/wiki/ISO_8601#Time_intervals '
                'For example: 2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
            )
            raise HttpError(HTTPStatus.BAD_REQUEST, message)
        start, end = enforce_timezone(start), enforce_timezone(end)
        return "time >= '{}' AND time <= '{}'".format(start, end)

    @property
    def registration(self):
        registration = self.get('registration', None)
        if registration is not None:
            return "registration='{}'".format(registration)
        else:
            return 'true'  # Inoffensive default.
