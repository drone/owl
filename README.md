# Owl

Python service that returns drone positions via an HTTP API.

See the [meta respository](https://framagit.org/drone/meta) to undestand
the big picture.

## Constraints

Be fast, be async.

## Install

Once the PostgreSQL with PostGIS extension is filled by Squirrel:

```
python3 server.py
```

## Timezone management strategy

* Use timezone aware timestamp columns in PostgreSQL.
* Do not care about server timezone.
* Always pass UTC timezone aware values to psycopg.
* Always retrieve UTC timezone aware values from PostgreSQL.
* Remember to always compare dates in the same timezones in SQL queries.

Add +1 here if you fall into on of these traps. Change strategy at +10.
