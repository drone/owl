import json
from datetime import datetime, timedelta, timezone

import pytest

pytestmark = pytest.mark.asyncio


async def test_positions_returns_200(client, bbox):
    response = await client.get('/positions/?{}'.format(bbox))
    assert response.status == b'200 OK'
    assert response.body == '{"data":[]}'


async def test_positions_returns_json(client, bbox):
    response = await client.get('/positions/?{}'.format(bbox))
    assert json.loads(response.body) == {"data": []}


async def test_positions_returns_positions(factory, client, bbox):
    payload = await factory()
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert data[0]['coordinates'] == payload['coordinates']
    assert data[0]['time'] == int(payload['time'].timestamp())
    assert data[0]['registration'] == payload['registration']
    assert data[0]['allowed'] is True


async def test_positions_returns_forbidden_positions(factory, client, bbox):
    await factory(allowed=False)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert data[0]['allowed'] is False


async def test_unchecked_positions_do_not_return_allowed_key(factory, client,
                                                             bbox):
    await factory(allowed_at=None)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert 'allowed' not in data[0]


async def test_positions_filtered_by_allowance_true(factory, client, bbox):
    await factory()
    await factory(allowed=False)
    await factory(allowed=None)
    response = await client.get('/positions/?allowed=true&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['allowed'] is True


async def test_positions_filtered_by_allowance_false(factory, client, bbox):
    await factory()
    await factory(allowed=False)
    await factory(allowed=None)
    response = await client.get('/positions/?allowed=false&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['allowed'] is False


async def test_positions_filtered_by_allowance_unknown(factory, client, bbox):
    await factory()
    await factory(allowed=False)
    await factory(allowed=None)
    response = await client.get('/positions/?allowed=null&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['allowed'] is None


async def test_old_positions_are_not_retrieved(factory, client, bbox):
    old = datetime.now(timezone.utc) - timedelta(seconds=31)
    await factory(time=old)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_fresh_positions_are_retrieved(factory, client, bbox):
    fresh = datetime.now(timezone.utc) - timedelta(seconds=29)
    await factory(time=fresh)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1


async def test_tracks_are_retrieved(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['coordinates'] == payload['coordinates']
    assert items[0]['time'] == payload['time'].timestamp()
    assert items[0]['allowed'] is True


async def test_tracks_are_retrieved_for_a_registration(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0))
    await factory(time=datetime(2017, 3, 1, 14, 0, 0), registration='bar')
    await factory(time=datetime(2017, 3, 1, 14, 0, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&registration={}&{}'.format(
            interval, payload['registration'], bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 2


async def test_tracks_are_retrieved_by_allowed_unknown(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0), allowed=False)
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0), allowed=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&allowed=null&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is None


async def test_tracks_are_retrieved_by_allowance_true(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0), allowed=False)
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0), allowed=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&allowed=1&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is True


async def test_tracks_are_retrieved_by_allowance_false(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0), allowed=False)
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0), allowed=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&allowed=off&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is False


async def test_unallowed_tracks_are_retrieved(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0), allowed=False)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is False


async def test_unchecked_tracks_dont_return_allowed_key(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0),
                            allowed_at=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert 'allowed' not in items[0]


async def test_tracks_at_start_boundaries_are_retrieved(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 1, 13, 0, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert payload['registration'] in data


async def test_tracks_at_end_boundaries_are_retrieved(factory, client, bbox):
    payload = await factory(time=datetime(2017, 3, 2, 13, 30, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert payload['registration'] in data


async def test_tracks_out_of_boundaries_arent_retrieved(factory, client, bbox):
    await factory(time=datetime(2017, 3, 2, 13, 31, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_positions_one_position_per_registration(factory, client, bbox):
    now = datetime.now(timezone.utc)
    previous = now - timedelta(seconds=29)
    await factory(time=previous, registration='same')
    await factory(coordinates=[2.3666, 48.8666, 0],
                  time=now, registration='same')
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['coordinates'] == [2.3666, 48.8666, 0]


async def test_tracks_many_positions_per_registration(factory, client):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0),
                            coordinates=[1.5, 47.5, 0])
    payload2 = await factory(time=datetime(2017, 3, 1, 14, 30, 0),
                             coordinates=[1.6, 47.6, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert items[0]['coordinates'] == payload['coordinates']
    assert items[1]['coordinates'] == payload2['coordinates']


async def test_tracks_shouldnt_consider_position_out_of_bbox(factory, client):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0),
                            coordinates=[1.5, 47.5, 0])
    await factory(time=datetime(2017, 3, 1, 14, 30, 0),
                  coordinates=[1.6, 47.6, 0])
    await factory(time=datetime(2017, 3, 1, 14, 30, 0),
                  coordinates=[1.6, 48.6, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert len(items) == 2


async def test_tracks_many_positions_per_registration_except_time(factory,
                                                                  client):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0),
                            coordinates=[1.5, 47.5, 0])
    await factory(time=datetime(2017, 3, 1, 14, 30, 0),
                  coordinates=[1.6, 47.6, 0])
    await factory(time=datetime(2017, 3, 2, 14, 30, 0),
                  coordinates=[1.6, 47.7, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert len(items) == 2


async def test_tracks_should_group_positions_per_registration(factory, client):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0),
                            coordinates=[1.5, 47.5, 0])
    await factory(time=datetime(2017, 3, 1, 14, 30, 0),
                  coordinates=[1.6, 47.6, 0])
    await factory(time=datetime(2017, 3, 1, 14, 30, 0),
                  coordinates=[1.6, 47.7, 0],
                  registration='test_bar')
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert len(items) == 2
    assert 'test_bar' in data
    assert len(data['test_bar']) == 1


async def test_bbox_positions_are_retrieved(factory, client):
    await factory(coordinates=[1.5, 47.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2&north=48')
    data = json.loads(response.body)['data']
    assert len(data) == 1


async def test_position_bbox_parameters_are_mandatory(factory, client):
    await factory(coordinates=[1.5, 47.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2')
    assert response.status == b'400 Bad Request'
    assert 'A boundary box is required' in response.body


async def test_position_bbox_parameters_are_wrong(factory, client):
    await factory(coordinates=[1.5, 47.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2&north=100')
    assert response.status == b'400 Bad Request'
    assert '`north` parameter must be within the [-90, 90]' in response.body


async def test_out_of_bbox_positions_are_not_retrieved(factory, client):
    await factory(coordinates=[2.5, 48.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2&north=48')
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_bbox_tracks_requires_interval(factory, client):
    await factory(coordinates=[1.5, 47.5, 0])
    response = await client.get('/tracks/?west=1&south=47&east=2&north=48')
    assert response.status == b'400 Bad Request'


async def test_tracks_within_bbox_are_retrieved(factory, client):
    payload = await factory(time=datetime(2017, 3, 1, 14, 0, 0),
                            coordinates=[1.5, 47.5, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert payload['registration'] in data


async def test_tracks_without_bbox_are_not_retrieved(factory, client):
    await factory(time=datetime(2017, 3, 1, 14, 0, 0),
                  coordinates=[2.5, 48.5, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_tracks_off_boundaries_in_bbox_not_retrieved(factory, client):
    await factory(time=datetime(2017, 3, 2, 14, 0, 0),
                  coordinates=[1.5, 47.5, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_tracks_interval_parameters_with_injection(factory, client):
    resp = await client.get('/tracks/?interval=drop%20table%20data')
    assert resp.status == b'400 Bad Request'
    assert '`interval` parameter must be a valid time interval' in resp.body


async def test_positions_bbox_parameters_with_injection(factory, client):
    resp = await client.get(
        '/positions/?west=drop%20table%20data&south=47&east=2&north=48')
    assert resp.status == b'400 Bad Request'
    assert resp.body == '{"error":"Key \'west\' must be castable to float"}'


async def test_tracks_bbox_parameters_with_injection(factory, client):
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    resp = await client.get((
        '/tracks/?interval={}'
        '&west=drop%20table%20data&south=47&east=2&north=48').format(interval))
    assert resp.status == b'400 Bad Request'
    assert resp.body == '{"error":"Key \'west\' must be castable to float"}'


async def test_reports_invalid_hash(client):
    response = await client.get('/report/foo')
    assert response.status == b'400 Bad Request'
    assert response.body == '{"error":"Invalid report hash"}'


async def test_reports_unknown_hash(client):
    response = await client.get('/report/{}'.format('*' * 32))
    assert response.status == b'404 Not Found'
    assert response.body == '{"error":"Unknown report hash"}'


async def test_reports_with_error(client, report):
    key = '*' * 32
    await report(key=key, errors={'foo': 'bar'})
    response = await client.get('/report/{}'.format(key))
    assert response.status == b'200 OK'
    payload = json.loads(response.body)
    assert payload['error'] == '{"foo": "bar"}'


async def test_report(client, report):
    await report(errors={'foo': 'bar'})
    response = await client.get('/report')
    assert response.status == b'200 OK'
    payload = json.loads(response.body)
    assert payload['data'][0] == {
        'data': "b''",
        'error': '{"foo": "bar"}'
    }


async def test_report_return_20_reports_by_default(client, report):
    key = '*' * 30
    # We should only retrieve 20 reports among the 21 created.
    for num in range(0, 21):
        await report(key='{}{:02x}'.format(key, num), errors='{}'.format(num))
    response = await client.get('/report')
    assert response.status == b'200 OK'
    payload = json.loads(response.body)
    # Verify that there are 20 reports returned
    assert len(payload['data']) == 20


async def test_report_return_last_reports_first(client, report):
    key = '*' * 31
    for num in range(0, 5):
        await report(key='{}{}'.format(key, num), errors='{}'.format(num))
    response = await client.get('/report')
    assert response.status == b'200 OK'
    payload = json.loads(response.body)
    for num in range(0, 5):
        assert payload['data'][num]['error'] == '"{}"'.format(4 - num)
